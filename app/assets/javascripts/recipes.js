  $( document ).ready(function() {

// Delay function to improve customer experience. Don't submit when keyp immediately
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

// Click on search button on key up with delay
$('#recipes_search_field').keyup(function() {
  delay(function(){
    $("#search_button").click();
  }, 500 );
});

});