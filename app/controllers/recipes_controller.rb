class RecipesController < ApplicationController
	
	def index
		# Get recipes from the Recipe Puppy API with the search params
		@recipes = RecipePuppy.new.recipes(params[:search])
		# Render HTML and Ajax Response
		respond_to do |format|
			format.html
			format.js
		end
	end
end