class RecipePuppy

# A service that fetches recipes from recipe puppy API using HTTPParty
	require 'open-uri'
	require 'httparty'

	def recipes(query=nil)
		page = 1
		results = []
		while true
			response = HTTParty.get("http://www.recipepuppy.com/api/?q=#{query}&p=#{page}")
			json = JSON.parse(response.body)
			json["results"].each do |result|
				results.push(result)
			end
			# Solution to present 20 results as the api default is 10
			break if page == 2
			page +=1
		end
		results
	end

end